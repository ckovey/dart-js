$(function() {

  // send html input to dart app
  $('#setBtn').click(function(e) {
    var event = new CustomEvent('setName', {detail: {name: $('#setInput').val()}});
    document.dispatchEvent(event);
  });

  // listen to dart app for name changes
  document.addEventListener('nameChanged', function(event){
    var pirateName = jQuery.parseJSON(event.detail);
    $('#getInput').val(pirateName.firstName);
  });

});
